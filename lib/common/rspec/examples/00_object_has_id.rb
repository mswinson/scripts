shared_examples 'an object that has id' do |class_id|
  id_path = class_id.to_s.split('.')
  provider_id = id_path[-1].to_sym

  describe('a named class') do
    describe('including') do
      describe('mixin HasId') do
        describe('provider_id') do
          it('defaults to class name') do
            expect(subject.provider_id).to eql(provider_id)
          end
        end

        describe('guid') do
          it('defaults to class path') do
            expect(subject.guid).to eql(class_id.to_sym)
          end
        end
      end
    end
  end
end
