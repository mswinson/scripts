module DOM
  # node is base class for all DOM nodes
  class Node
    include Common::Mixins::HasId
    include Common::Mixins::HasDescendants
    include Common::Mixins::IsAFactory
    include Common::Mixins::HasProviders
    include Mixins::HasAttributes
    include Mixins::Serialize::AsJSON

    private

    # initialize node
    # @api private
    # @param attributes [Hash] attributes
    def initialize(attributes = nil)
      attributes ||= {}

      init_attributes(attributes)
    end
  end
end
