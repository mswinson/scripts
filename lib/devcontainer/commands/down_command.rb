module Devcontainer
  module Commands
    # stand down devcontainer
    class DownCommand < ShellCommand
      # run command
      # @api public
      # @param env [OpenStruct] context
      # @param args [Array] command arguments
      # @return void
      def run(env, args)
        devcontainer = env.workspace

        devcontainer_path = env.config.devcontainer_path
        Dir.chdir(devcontainer_path) do
          docker_compose_path = devcontainer['dockerComposeFile']
          cmd "docker-compose -f #{docker_compose_path} down"
        end
      end
    end
  end
end
