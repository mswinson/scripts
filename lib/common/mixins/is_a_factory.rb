module Common
  module Mixins
    # mixin IsFactory adds class factory support
    # @example
    #   class MyClass
    #     include IsAFactory
    #   end
    #   class SubClass < MyClass
    #   end
    #
    #   instance = MyClass.create(:subclass, {})
    #   instance.class
    #   => SubClass
    #
    # @example
    #   instance = MyClass.create({'type' => :subclass})
    #   instance.class
    #   => SubClass
    #
    # @example
    #   instance = MyClass.create(SubClass.new)
    #   instance.class
    #   => SubClass
    module IsAFactory
      # include factory support
      # @api public
      # @param base [Class] host class
      # @return void
      def self.included(base)
        base.extend(ClassMethods)
      end

      # include class level methods
      module ClassMethods
        # create subclass by id
        # @api public
        # @overload create(yype)
        #   @param type [String] type string
        #   @return [Object]
        # @overload create(attributes)
        #   @param attributes [Hash] type annotated attributes
        #   @option attributes [String] :type type id (required)
        #   @return [Object]
        #   @raise ArgumentError - if class not found
        # @overload create(obj)
        #   @param obj [Object] instance of this class
        #   @return [obj]
        def create(type)
          if type.kind_of?(self)
            type
          else
            factory_method = "from_#{type.class.name.downcase}"
            unless respond_to?(factory_method)
              raise(TypeError, 'invalid type passed to create')
            end

            __send__(factory_method, type)
          end
        end
      end
    end
  end
end
