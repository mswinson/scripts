module Workspace
  module Commands
    class ExportCommand < Command
      # run command
      # @api public
      # @param ws [Hash] workspace context
      # @param args [Array<String>] command arguments
      # @return void
      def run(ws, args)
        cmd = args.shift

        case cmd
        when 'vscode'
          vscode(ws, args)
        end
      end

      private

      # export vscode workspace
      # @api private
      # @param ws [Hash] workspace context
      # @param args [Array<String>] command arguments
      # @return void
      def vscode(ws, args)
        h = {
          'folders' => []
        }

        repos = ws.repos
        repos.each do |repo|
          name = repo.name
          dest =  repo.dest
          path = "#{dest}/#{name}"

          h['folders'] << {
            name: path,
            path: path
          }
        end

        puts JSON.pretty_generate(h)
      end
    end
  end
end
