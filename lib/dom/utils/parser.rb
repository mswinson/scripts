module DOM
  module Utils
    # parser is base clas for all DOM parsers
    class Parser < Node
      attr_accessor :source

      # from data type
      # @api public
      # @param type [String] data type
      # @return [Parser]
      def self.from_data_type(type)
        descendants.find do |descendant|
          descendant.consumes.include?(type)
        end
      end

      # attributes
      # @api public
      # @return [Hash]
      def self.attributes
        {
          source: nil
        }
      end

      # list of data types this parser consumes
      # @api public
      # @return [Array<String>]
      def self.consumes
        []
      end

      # parse data
      # @api public
      # @return [DOM::Element]
      def parse
        raise(NotImplementedError)
      end
    end
  end
end
