module Machine
  module Commands
    class DockerCommand < ShellCommand
      protected

      # machine environment
      # @api private
      # @param machine [Hash] machine context
      # @return [Hash]
      def machine_env_for(machine)
        docker_env = docker_env_for(machine)
        docker_host_uri = URI(docker_env['DOCKER_HOST'])
        remote_host = docker_host_uri.host

        env = {}
        env = env.merge(docker_env)
        env['REMOTE_USER'] = machine['options']['amazonec2-ssh-user'] || 'ubuntu'
        env['REMOTE_HOST'] = remote_host
        env['SSH_KEY'] = machine['options']['amazonec2-keypair-name'] || ''
        env
      end

      # docker environment
      # @api private
      # @param machine [Hash] machine context
      # @return [Hash]
      def docker_env_for(machine)
        name = machine['name']

        env_output = docker_machine("env", {}, name, true)
        
        env_hash = {}
        env_output.split("\n").each do |line|
          next if line[0] == '#'

          env_line = line.gsub(/^export /,'')
          key,value = env_line.split('=')
          env_hash[key] = value.gsub('"','')
        end
        env_hash
      end

      # run docker machine
      # @api privagte
      # @param cmd [String] docker machine command
      # @param options [Hash] options
      # @param name [String] machine name
      # @param output [Boolean] capture output
      # @return void
      def docker_machine(cmd, options,name, output=false)
        args = [
          "docker-machine",
          cmd
        ]

        options.each do |key, value|
          args << "--#{key}"
          args << value
        end

        args << name

        cmd(args, output)
      end
    end
  end
end
