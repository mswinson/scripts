module DOM
  # NodeMap is a map of Nodes
  class NodeMap
    # create collection
    # @api public
    # @param list [Enumerable] collection
    # @return [NodeList]
    def self.create(map)
      return unless map.respond_to?(:to_h)

      instance = new
      map.to_h.each do |key, value|
        instance[key] = value
      end
      instance
    end

    # create subclass
    # @api public
    # @return [Class]
    def self.of(item)
      klass = Class.new(self)
      klass.define_singleton_method(:item_class) do
        item
      end
      klass
    end

    # map class
    # @api public
    # @return [Hash]
    def self.map_class
      Hash
    end

    # item c;ass
    # @api public
    # @return [Class]
    def self.item_class
      Node
    end

    # set key
    # @api public
    # @param key [String] key
    # @param value [Object] value
    # @return [Object]
    def []=(key, value)
      item = create_item_from(value)
      map[key.to_sym] = item
    end

    # get value
    # @api public
    # @param key [String] key
    # @return [Node]
    def [](key)
      map[key.to_sym]
    end

    # serialize as json
    # @api public
    # @return [Hash]
    def as_json
      h = {}
      map.each do |key, value|
        h[key.to_sym] = if value.respond_to?(:as_json)
                          value.as_json
                        else
                          value
                        end
      end
      h
    end

    # rubocop:disable Style/MethodMissingSuper

    # forward methods
    # @api public
    # @return void
    def respond_to_missing?(m)
      methods = [
        'as_json',
        '[]=',
        '[]'
      ]
      methods.include?(m) || map.respond_to?(m)
    end

    # forward methods
    # @api public
    # @return void
    def method_missing(m, *args, &block)
      map.public_send(m, *args, &block) if map.respond_to?(m)
    end

    # rubocop:enable Style/MethodMissingSuper

    private

    # create item from object
    # @api private
    # @param obj [Object] object
    # @return [Object]
    def create_item_from(obj)
      if self.class.item_class.respond_to?(:create)
        self.class.item_class.create(obj)
      else
        obj
      end
    end

    # map store
    # @api private
    # @return [Hash]
    def map
      @map ||= self.class.map_class.new
      @map
    end
  end
end
