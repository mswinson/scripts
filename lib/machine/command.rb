module Machine
  class Command
    # register subcommand
    # @api public
    # @param base [Class] subclass
    def self.inherited(subclass)
      subclasses << subclass
    end

    # store subclasses
    # @api public
    # @return [Array<Class>]
    def self.subclasses
      @subclasses ||= []
      @subclasses
    end

    # all descendants
    # @api public
    # @return [Array<Class>]
    def self.descendants
      d = []

      subclasses.each do |klass|
        if !klass.descendants.empty?
          d << klass.descendants
        else
          d << klass
        end
      end

      d.flatten
    end

    # find command
    # @api public
    # @param name [String] command name
    # @return [Command]
    def self.find_provider(name)
      descendants.find do |klass|
        klass_id = klass.name.split("::").last.downcase
        provider_id = klass_id.gsub(/command$/, "")
        provider_id == name.downcase
      end
    end
  end
end
