shared_examples 'an object that is a factory' do
  describe('create') do
    context('with class data in a hash') do
      let(:class_id) { :my }
      let(:class_data) { { type: class_id, data: {} } }

      before(:each) do
        allow(subject).to receive(:from_hash)
      end

      before(:each) do
        subject.create(class_data)
      end

      it('calls from_hash') do
        expect(subject).to have_received(:from_hash).with(class_data)
      end
    end
  end
end
