require 'uri'
require 'fileutils'
module DOM
  module Utils
    module Loaders
      # file loader loads dom elements from file source
      class FileLoader < Loader
        attr_accessor :url

        # parser class
        # @api public
        # @return  [Class]
        def self.parser_class
          Parser
        end

        # attributes
        # @api public
        # @return [Hash]
        def self.attributes
          {
            url: nil
          }
        end

        # consumes
        # @api public
        # @return [Array<String>]
        def self.consumes
          [
            %r{^(file://).*},
            %r{^(?![a-z]+://)[\.A-Za-z0-9]*(/[A-Za-z0-9]+)*(\.[A-Za-z0-9]+)}
          ]
        end

        # load
        # @api public
        # @return [DOM::Element]
        # @raise [RuntimeError] if parser not found
        # @raise [Err::ENOENT] if file not found
        def load
          source = File.read(path)
          parse(source)
        end

        private

        # parse source
        # @api private
        # @param source [String] source data
        # @return [DOM::Element]
        # @raise [RuntimeError] if parser not found
        def parse(source)
          raise("Parser not found for path #{path}") unless parser_class

          parser = parser_class.new(source: source)
          parser&.parse
        end

        # path component
        # @api private
        # @return [String]
        def path
          @path ||= URI(url).path
          @path
        end

        # create parser from path
        # @api private
        # @return [Class]
        def parser_class
          return @parser_class if @parser_class

          if datatype
            @parser_class = self.class.parser_class.from_data_type(datatype)
          else
            @parser_class = nil
          end
        end

        # parse data type from path
        # @api private
        # @return [String]
        def datatype
          return @datatype if @datatype

          fileext = File.extname(path)
          fileext = fileext[1..-1]

          @datatype = "fileext:#{fileext}" if fileext
          @datatype
        end
      end
    end
  end
end
