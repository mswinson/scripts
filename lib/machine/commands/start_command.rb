module Machine
  module Commands
    class StartCommand < DockerCommand
      # run command
      # @api public
      # @param env [OpenStruct] environment
      # @param args [Array<String>] command args
      # @return void
      def run(env, args)
        machine = env.machine

        name = machine['name']
        options = {}
        ok = docker_machine("start", options, name)
        if ok
          # regenerate certs
          log "regenerating certs"
          docker_machine("regenerate-certs", {}, name)

          # generate env
          log "generating environment"
          machine_env = machine_env_for(machine)
          env.state = machine_env
        end
      end
    end
  end
end
