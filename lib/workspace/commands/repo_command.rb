module Workspace
  module Commands
    class RepoCommand < ShellCommand
      # run command
      # @api public
      # @param ws [Hash] workspace context
      # @param args [Array<String>] command arguments
      # @return void
      def run(ws, args)
        selector = args.shift
        
        selected = ws.repos.select do |repo|
          repo.name =~ /#{selector}/
        end

        cmd = args.shift

        selected.each do |repo|
          case cmd
          when '--ls'
            ls(repo, args)
          when '--exec'
            exec(repo, args)
          when '--clone'
            clone(repo, args)
          end
        end
      end

      private

      # execute command in repo context
      # @api private
      # @param repo [Hash] repository context
      # @param args [Array<String>] arguments
      # @return void
      def exec(repo, args)
        name = repo.name
        dest = repo.dest
        path = "#{dest}/#{name}"

        Dir.chdir(path) do
          cmd(args)
        end
      end

      # clone repo
      # @api private
      # @param repo [Hash] repository context
      # @param args [Array<String>] arguments
      # @return void
      def clone(repo, args)
        name = repo.name
        source = repo.source
        dest = repo.dest

        source_path = "#{source}/#{name}"
        dest_path = "#{dest}/#{name}"

        if Dir.exist?(dest_path) 
          puts "repository #{name} already exists at #{dest_path}"
        else
          cmd(["git", "clone", source_path, dest_path])
        end
      end

      # list repos
      # @api private
      # @param repo [Hash] repository context
      # @param args [Array<String>] arguments
      # @return void
      def ls(repo, args)
        name = repo.name
        dest = repo.dest

        puts "#{name}, #{dest}/#{name}"
      end
    end
  end
end
