module Devcontainer
  module DOM
    module Utils
      class Loader
        attr_reader :path

        # load devcontainer
        # @api public
        # @return [Hash]
        def self.load(path)
          new(path).load
        end

        # load data
        # @api public
        # @return [Hash]
        def load
          source = File.read(devcontainer_path)
          data = JSON.parse(source)
          data
        end

        private

        # devcontainer json 
        # @api private
        # @return [String]
        def devcontainer_path
          "#{path}/devcontainer.json"
        end

        # intialize loader
        # @api private
        # @param path [String] file path
        def initialize(path)
          @path = path
        end
      end
    end
  end
end
