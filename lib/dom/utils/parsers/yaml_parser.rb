require 'yaml'

module DOM
  module Utils
    module Parsers
      # parse yaml data
      class YamlParser < Parser
        # consumes yaml file extension
        # @api public
        # @return [Array<String>]
        def self.consumes
          [
            'fileext:yml',
            'fileext:yaml'
          ]
        end

        # parse
        # @api public
        # @return [DOM::Element]
        def parse
          data = YAML.safe_load(source)
          case data
          when Array
          when Hash
            h = {}
            data.each do |key, value|
              value['type'] ||= key
              h[key] = DOM::Element.create(value)
            end
            h
          end
        end
      end
    end
  end
end

