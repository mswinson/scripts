module Devcontainer
  module Commands
    # base command for shell base commands
    class ShellCommand < Command
      # log messages
      # @api public
      # @param message [String] message
      # @return void
      def log(message)
        puts message
      end

      # shell command
      # @api public
      # @param args [Array<String>] command args
      # @param output [Boolean] generate output
      def cmd(args,output=false)
        command = if args.respond_to?(:to_a)
                    args.join(' ')
                  else
                    args
                  end

        if output
          `#{command}`
        else
          system(command)
        end
      end
    end
  end
end
