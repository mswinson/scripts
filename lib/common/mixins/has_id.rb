module Common
  module Mixins
    # add support for class id to class
    # @example - with named class
    #   module Namespace
    #     class MyClass
    #       include HasId
    #     end
    #     Namespace::MyClass.provider_id
    #     => :myclass
    #     Namespace::MyClass.guid
    #     => :"namespace.myclass"
    #
    # @example - with anonymous class
    #   k = Class.new
    #   k.include(HasId)
    #   k.name
    #   => nil
    #   k.provider_id
    #   => 12345
    module HasId
      # include class id support
      # @api public
      # @param base [Class] host class
      # @return void
      def self.included(base)
        base.extend(ClassMethods)
      end

      # include class level methods
      module ClassMethods
        # provider id
        # @api public
        # @param id [String] class id
        # @return [Symbol]
        def provider_id(id = nil)
          @id ||= default_provider_id
          @id = id.downcase.to_sym if id
          @id
        end

        # namespace id
        # @api public
        # @return [Symbol]
        def ns_id
          @ns_id ||= superclass.guid if superclass&.respond_to?(:guid)
          @ns_id
        end

        # class guid
        # @api public
        # @return [Symbol]
        def guid
          @guid ||= [ns_id, provider_id].compact.join('.').to_sym
          @guid
        end

        private

        # default provider id
        # @api private
        # @return [Symbol]
        def default_provider_id
          if name.nil?
            object_id.to_s
          else
            name.split(/::/).last.downcase.to_sym
          end
        end
      end

      # provider id - instance helper
      # @api public
      # @return [Symbol]
      def provider_id
        self.class.provider_id
      end
    end
  end
end
