module Machine
  module Commands
    class EnvCommand < DockerCommand
      # run command
      # @api public
      # @param env [OpenStruct] environment
      # @param args [Array<String>] command args
      # @return void
      def run(env, args)
        machine = env.machine

        machine_env = machine_env_for(machine)
        puts machine_env
      end
    end
  end
end
