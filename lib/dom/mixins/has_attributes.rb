module DOM
  module Mixins
    # mixin HasAttributes adds support for attributes
    # @example
    #   class MyClass
    #     include HasAttributes
    #
    #     def self.attributes
    #       {
    #         'attr1' => nil,
    #         'attr2' => nil
    #       }
    #     end
    #   end
    #   MyClass.attributes
    #   => { 'attr1' => nil, 'attr2' => nil }
    module HasAttributes
      # include attribute support
      # @api public
      # @param base [Class] host class
      # @return void
      def self.included(base)
        base.extend(ClassMethods)
      end

      # class methods
      module ClassMethods
        # default attributes
        # @api public
        # @return [Hash]
        def default_attributes
          {}
        end

        # attributes
        # @api public
        # @return [Hash]
        def attributes
          {}
        end
      end

      # default attributes - instance helper
      # @api public
      # @return [Hash]
      def default_attributes
        self.class.default_attributes
      end

      # attributes - instance helper
      # @api public
      # @return [Hash]
      def attributes
        self.class.attributes
      end

      # attributes to hash
      # @api public
      # @return [Hash]
      def to_h
        h = {}
        attributes.each_key do |key|
          value = __send__(key)
          h[key.to_sym] = value
        end
        h
      end

      # update attributes
      # @api public
      # @param attributes [Hash] attributes
      # @return void
      def update_attributes(attributes)
        attributes.each do |k, v|
          __setattr__(k, v)
        end
      end

      # initialize attributes
      # @api public
      # @param attributes [Hash] attributes
      # @return void
      def init_attributes(attributes)
        merged_attributes = merge_defaults(attributes)
        update_attributes(merged_attributes)
      end

      protected

      # merge default attributes
      # @api public
      # @param attributes [Hash] attributes
      # @return [Hash]
      def merge_defaults(attributes)
        default_attributes.merge(attributes)
      end

      # set attribute
      # @api private
      # @param k [String] key
      # @param v [Object] value
      # @return void
      def __setattr__(k, v)
        return unless attributes.key?(k) || attributes.key(k.to_sym)
        return unless respond_to?("#{k}=")

        __send__("#{k}=", v)
      end
    end
  end
end
