module Common
  # common mixins
  module Mixins
  end
end

require 'common/mixins/has_id'
require 'common/mixins/has_descendants'
require 'common/mixins/has_providers'
require 'common/mixins/is_a_factory'
