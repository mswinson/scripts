module Common
  module Mixins
    # adds provider support to class
    module HasProviders
      # include provider support
      # @api public
      # @param base [Class] host class
      # @return void
      def self.included(base)
        base.include(HasId)
        base.include(HasDescendants)
        base.include(IsAFactory)
        base.extend(ClassMethods)
      end

      # include class methods
      module ClassMethods
        # provider class
        # @api private
        # @return [Class]
        def provider_class
          self
        end

        # has providers
        # @api private
        # @return [Booelean]
        def providers?
          descendants?
        end

        # find provider by id
        # @api private
        # @param type [String] type id
        # @return [Class]
        def find_provider_by_id(type)
          id = "#{type}#{provider_classname}".downcase
          find_descendants(id)
        end

        # provider class name
        # @api private
        # @return [String]
        def provider_classname
          name.split('::').last.downcase
        end

        # create from symbol
        # @api private
        # @param type [Symbol] type
        # @return [Object]
        # @raise ArgumentError - if class not found
        def from_symbol(type)
          from_string(type.to_s)
        end

        # create from string
        # @api private
        # @param type [String] type
        # @return [Object]
        # @raise ArgumentError - if class not found
        def from_string(type)
          data = {
            type: type
          }
          from_hash(data)
        end

        # create from hash
        # @api private
        # @param data [Hash] object data
        # @return [Object]
        # @raise ArgumentError - if class not found
        def from_hash(data)
          type = data['type'] || data[:type]
          klass = if type && providers?
                    find_provider_by_id(type)
                  else
                    self
                  end
          klass&.new(data)
        end
      end
    end
  end
end
