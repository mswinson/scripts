module Machine
  module Commands
    class ShellCommand < Command
      # log messages
      # @api public
      # @param message [String] message
      # @return void
      def log(message)
        puts message
      end

      # shell command
      # @api public
      # @param args [Array<String>] command args
      # @param output [Boolean] generate output
      def cmd(args,output=false)
        command = args.join(' ')

        if output
          `#{command}`
        else
          system(command)
        end
      end
    end
  end
end
