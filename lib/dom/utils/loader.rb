module DOM
  module Utils
    # loader is base class from DOM loaders
    class Loader < Node
      attr_accessor :url

      # attributes
      # @api public
      # @return [Hash]
      def self.attributes
        {
          url: nil
        }
      end

      # load from url
      # @api public
      # @param url [String] url
      # @return [DOM::Element]
      def self.load(url)
        loader = loader_for(url)
        return loader.load if loader

        raise("Loader not found for url #{url}")
      end

      # loader for url
      # @api public
      # @param url [String] url
      # @return [Loader]
      def self.loader_for(url)
        loader_class = loader_class_for(url)
        loader_class&.new(url: url)
      end

      # find loader class
      # @api public
      # @param url [String] url
      # @return [Class]
      def self.loader_class_for(url)
        descendants.find do |descendant|
          descendant.consumes?(url)
        end
      end

      # load
      # @api public
      # @return void
      def load
        raise(NotImplementedError)
      end

      # consumes?
      # @api public
      # @return [Boolean]
      def self.consumes?(url)
        path = url.to_s

        consumes.any? do |regex|
          m = path.match(regex)
          !m.nil?
        end
      end

      # list of urls this loader consumes
      # @api public
      # @return [Array<String>]
      def self.consumes
        []
      end
    end
  end
end
