module Machine
  module DOM
    module Utils
      class Loader
        attr_reader :path

        # load machine
        # @api public
        # @return [Hash]
        def load
          source = File.read(path)
          data = YAML.load(source)
          workspace = data['machine'] || {}
          workspace
        end

        private

        # initialize laoder
        # @api private
        # @param path [String] workspace path
        def initialize(path)
          @path = path
        end
      end
    end
  end
end
