module DOM
  # dom mixins
  module Mixins
  end
end

require 'dom/mixins/has_attributes'
require 'dom/mixins/serialize_as_json'
