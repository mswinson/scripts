example_files = Dir.glob(::File.expand_path('../examples/*.rb', __FILE__)).sort
example_files.each { |f| require_relative f }
