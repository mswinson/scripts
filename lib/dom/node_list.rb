module DOM
  # Nodelist is a list of nodes
  class NodeList
    # create collection
    # @api public
    # @param list [Enumerable] collection
    # @return [NodeList]
    def self.create(list)
      return unless list.respond_to?(:to_a)

      instance = new
      list.to_a.each do |item|
        instance << item
      end
      instance
    end

    # create subclass
    # @api public
    # @return [Class]
    def self.of(item)
      klass = Class.new(self)
      klass.define_singleton_method(:item_class) do
        item
      end
      klass
    end

    # list class
    # @api public
    # @return [Array]
    def self.list_class
      Array
    end

    # item class
    # @api public
    # @return [Class]
    def self.item_class
      ::DOM::Node
    end

    # append item
    # @api public
    # @param obj [Node] node object
    # @return [NodeList]
    def push(obj)
      item = create_item_from(obj)
      list.push(item)
      self
    end

    # append item
    # @api public
    # @param obj [Node] node object
    # @return [NodeList]
    def append(obj)
      push(obj)
    end

    # append item
    # @api public
    # @param obj [Node] node object
    # @return [NodeList]
    def <<(obj)
      push(obj)
    end

    # serialize
    # @api public
    # @return [Array]
    def as_json
      json = []
      list.each do |item|
        json << item.as_json if item.respond_to?(:as_json)
      end
      json
    end

    # forward methods
    # @api public
    # @return void
    def respond_to_missing?(m)
      methods = [
        'as_json',
        'push',
        '<<',
        'append'
      ]
      methods.include?(m) || list.respond_to?(m)
    end

    # rubocop:disable Style/MethodMissingSuper

    # forward methods
    # @api public
    # @return void
    def method_missing(m, *args, &block)
      list.public_send(m, *args, &block) if list.respond_to?(m)
    end

    # rubocop:enable Style/MethodMissingSuper

    private

    # set list
    # @api public
    # @return [Array]
    def list
      @list ||= self.class.list_class.new
      @list
    end

    # create item
    # @api private
    # @param obj [Node|Object]
    # @return void
    def create_item_from(obj)
      if self.class.item_class.respond_to?(:create)
        self.class.item_class.create(obj)
      else
        obj
      end
    end
  end
end
