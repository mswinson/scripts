module Devcontainer
  module Commands
    # stand up devcontainer
    class UpCommand < ShellCommand
      # run command
      # @api public
      # @param env [OpenStruct] environment
      # @param args [Array<String>] arguments
      # @return void
      def run(env, args)
        up(env, args)
        provision(env, args)
      end

      private

      # start container
      # @api private
      # @param env [OpenStruct] environment
      # @param args [Array<String>] arguments
      # @return void
      def up(env, args)
        devcontainer = env.workspace

        devcontainer_path = env.config.devcontainer_path
        Dir.chdir(devcontainer_path) do
          docker_compose_path = devcontainer['dockerComposeFile']

          service = devcontainer['service']
          workspace = devcontainer['workspaceFolder']
          user = devcontainer['remoteUser']

          cmd "docker-compose -f #{docker_compose_path} up -d"
        end
      end

      # provision container
      # @api private
      # @param env [OpenStruct] environment
      # @param args [Array<String>] arguments
      # @return void
      def provision(env, args)
        provision_command = ProvisionCommand.new
        provision_command.run(env, args)
      end
    end
  end
end
