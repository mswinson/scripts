module Machine
  module Commands
    class CreateCommand < DockerCommand
      # run command
      # @api public
      # @param env [OpenStruct] environment
      # @param args [Array<String>] command args
      # @return void
      def run(env, args)
        machine = env.machine

        name = machine['name']
        options = machine['options']
        options['driver'] = machine['driver']

        ok = docker_machine("create", options, name)
        if ok
          # generate env
          log "generating environment"
          machine_env = machine_env_for(machine)
          env.state = machine_env
        end
      end
    end
  end
end
