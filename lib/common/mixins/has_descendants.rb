module Common
  module Mixins
    # mixin HasDescendants adds class descendant tracking
    # @example
    #   class MyClass
    #     include Mixin::HasDescendants
    #   end
    #   class MySubClass < MyClass
    #   end
    #   class MySubSubClass < MySubClass
    #   end
    #   MyClass.direct_descendants
    #   => [ MySubClass ]
    #   MyClass.descendants
    #   => [ MySubClass, MySubSubClass]
    module HasDescendants
      # include class descendant support
      # @api public
      # @param base [Class] host class
      # @return void
      def self.included(base)
        base.include(HasId)
        base.extend(ClassMethods)
      end
    end

    # include class level methods
    module ClassMethods
      def inherited(klass)
        direct_descendants << klass
      end

      def direct_descendants
        @direct_descendants ||= []
        @direct_descendants
      end

      # descendants
      # @api public
      # @return [Array<Class>]
      def descendants
        d = []
        direct_descendants.each do |desc|
          d << desc
          if desc.descendants?
            d << desc.descendants
          end
        end
        d.flatten
      end

      # has descendants
      # @api public
      # @return [Boolean]
      def descendants?
        !descendants.empty?
      end

      # find subclass by id
      # @api public
      # @param id [String] relative id
      # @return [Class]
      # @example
      #   class TestClass
      #     include HasDescendants
      #   end
      #   class TestClass2 < TestClass
      #   end
      #   class TestClass3 < TestClass3
      #   TestClass.find_descendants(:testclass)
      #   => TestClass
      #   TestClass.find_descendants(:testclass2)
      #   => TestClass2
      #   TestClass.find_descendants("testclass2.testclass3")
      #   => TestClass3
      #   TestClass.find_descendants("abc")
      #   => nil
      def find_descendants(id)
        return self if provider_id == id.to_sym

        base_guid = guid
        descendants.find do |klass|
          klass.guid == "#{base_guid}.#{id}".to_sym
        end
      end
    end
  end
end
