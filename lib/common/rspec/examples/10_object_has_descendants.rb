shared_examples 'an object that has descendants' do
  before(:each) do
    class TestClass2 < subject
    end
    class TestClass3 < TestClass2
    end
  end

  after(:each) do
    Object.__send__(:remove_const, :TestClass3)
    Object.__send__(:remove_const, :TestClass2)
    subject.reset
  end

  describe('direct_descendants') do
    it('returns all immediate subclasses') do
      expect(subject.direct_descendants).to include(TestClass2)
    end
  end

  describe('descendants') do
    it('returns all subclasses') do
      expect(subject.descendants).to include(TestClass2, TestClass3)
    end
  end
end
