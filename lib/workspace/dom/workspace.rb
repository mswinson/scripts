module Workspace
  module DOM
    class WorkspaceElement < ::DOM::Element
      attr_accessor :name
      
      def self.attributes
        {
          'name' => nil,
          'repos' => nil
        }
      end

      def repos
        @repos ||= []
        @repos
      end

      def repos=(data)
        data.to_a.each do |item|
          item['type'] ||= 'repo'
          resource = ::DOM::Element.create(item)
          repos << resource
        end
        @repos
      end
    end
  end
end

