module Machine
  module Commands
    class KillCommand < DockerCommand
      # run command
      # @api public
      # @param env [OpenStruct] environment
      # @param args [Array<String>] command args
      # @return void
      def run(env, args)
        machine = env.machine

        name = machine['name']
        options = {}

        docker_machine("kill", options, name)

        env.state = {}
      end
    end
  end
end
