# rubocop:disable Metrics/BlockLength
# rubocop:disable Metrics/LineLength
shared_examples 'an object that has providers' do
  describe('create') do
    context('with valid provider data') do
      let(:provider_id) { :my }
      let(:provider_classname) { subject.provider_classname }
      let(:guid) { "#{subject.guid}.#{provider_id}#{provider_classname}".to_sym }
      let(:provider_class) do
        klass = double
        allow(klass).to receive(:provider_id).and_return(provider_id)
        allow(klass).to receive(:guid).and_return(guid)
        allow(klass).to receive(:new)
        klass
      end

      let(:descendants) do
        [
          provider_class
        ]
      end

      before(:each) do
        allow(subject).to receive(:descendants).and_return(descendants)
        allow(subject).to receive(:find_provider_by_id).and_call_original
      end

      context('with type') do
        context('in a hash') do
          let(:class_data) { { type: provider_id, data: {} } }

          before(:each) do
            allow(subject).to receive(:from_hash).and_call_original
          end

          before(:each) do
            subject.create(class_data)
          end

          it('calls from_hash') do
            expect(subject).to have_received(:from_hash).with(class_data)
          end

          it('finds provider') do
            expect(subject).to have_received(:find_provider_by_id).with(provider_id)
            expect(provider_class).to have_received(:guid)
          end

          it('calls new') do
            expect(provider_class).to have_received(:new)
          end
        end

        context('in a string') do
          before(:each) do
            allow(subject).to receive(:from_string).and_call_original
          end

          before(:each) do
            subject.create(provider_id.to_s)
          end

          it('calls from_string') do
            expect(subject).to have_received(:from_string).with(provider_id.to_s)
          end

          it('finds provider') do
            expect(subject).to have_received(:find_provider_by_id).with(provider_id.to_s)
            expect(provider_class).to have_received(:guid)
          end

          it('calls new') do
            expect(provider_class).to have_received(:new)
          end
        end

        context('in a symbol') do
          before(:each) do
            allow(subject).to receive(:from_symbol).and_call_original
          end

          before(:each) do
            subject.create(provider_id)
          end

          it('calls from_symbol') do
            expect(subject).to have_received(:from_symbol).with(provider_id)
          end

          it('finds provider') do
            expect(subject).to have_received(:find_provider_by_id).with(provider_id.to_s)
            expect(provider_class).to have_received(:guid)
          end

          it('calls new') do
            expect(provider_class).to have_received(:new)
          end
        end
      end
    end
  end
end
