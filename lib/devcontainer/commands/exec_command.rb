module Devcontainer
  module Commands
    # shell into devcontainer
    class ExecCommand < ShellCommand
      # run command
      # @api public
      # @param env [OpenStruct] application context
      # @param args [Array] command arguments
      # @return void
      def run(env, args)
        devcontainer = env.workspace

        devcontainer_path = env.config.devcontainer_path
        Dir.chdir(devcontainer_path) do
          docker_compose_path = devcontainer['dockerComposeFile']
          service = devcontainer['service']
          user = devcontainer['remoteUser']

          cmd "docker-compose -f #{docker_compose_path} exec -u #{user} #{service} /bin/bash"
        end
      end
    end
  end
end
