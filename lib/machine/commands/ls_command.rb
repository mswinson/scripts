module Machine
  module Commands
    class LSCommand < DockerCommand
      # run command
      # @api public
      # @param env [OpenStruct] environment
      # @param args [Array<String>] command args
      # @return void
      def run(env, args)
        machine = env.machine
        puts machine['name']
      end
    end
  end
end
