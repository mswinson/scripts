module DOM
  module Mixins
    module Serialize
      # serialize attributes as json
      module AsJSON
        # include json support
        # @api public
        # @param base [Class] host class
        # @return void
        def self.included(base)
          base.include(HasAttributes)
          base.include(InstanceMethods)
        end

        # instance methods
        module InstanceMethods
          # serialize attributes as json
          # @api public
          # @return [Hash]
          def as_json
            h = {}
            attributes.each_key do |key|
              next unless respond_to?(key)

              value = __send__(key)
              h[key.to_sym] = json_for(value)
            end
            h
          end

          private

          # return json for value
          # @api private
          # @param value [Object] object
          # @return [Hash|Object]
          def json_for(value)
            if value.respond_to?(:as_json)
              value.as_json
            else
              value
            end
          end
        end
      end
    end
  end
end
