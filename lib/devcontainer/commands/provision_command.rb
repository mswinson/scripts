module Devcontainer
  module Commands
    # provision devcontainer
    class ProvisionCommand < ShellCommand
      # run command
      # @api private
      # @param env [OpenStruct] environment
      # @param args [Array<String>] arguments
      # @return void
      def run(env, args)
        devcontainer = env.workspace

        devcontainer_path = env.config.devcontainer_path
        Dir.chdir(devcontainer_path) do
          docker_compose_path = devcontainer['dockerComposeFile']
          service = devcontainer['service']
          workspace = devcontainer['workspaceFolder']
          user = devcontainer['remoteUser']

          init_script = initscript_for(devcontainer)
          unless init_script.empty?
            cmd "docker-compose -f #{docker_compose_path} exec -u #{user} #{service} /bin/bash -c '#{init_script}'"
          end
        end
      end

      private

      # generate dotfiles install script
      # @api private
      # @param devcontainer [Hash] dev container
      # @return [Array<String]
      def dotfiles_commands_for(devcontainer)
        dotfiles_repo = "https://bitbucket.org/mswinson/dotfiles"
        dotfiles_path = "~/.dotfiles"
        dotfiles_install_script = "./install"

        [
          "git clone #{dotfiles_repo} #{dotfiles_path}",
          "cd #{dotfiles_path}",
          dotfiles_install_script
        ]
      end

      # pre setup commands
      # @api private
      # @param devcontainer [Hash] dev container
      # @return [Array<String>]
      def precommands_for(devcontainer)
        dotfiles_commands_for(devcontainer)
      end

      # post setup commands
      # @api private
      # @param devcontainer [Hash] dev container
      # @return [Array<String>]
      def postcommands_for(devcontainer)
        devcontainer['postCreateCommand'] || []
      end

      # initialization commands
      # @api private
      # @param devcontainer [Hash] dev container
      # @return [Array<String>]
      def initcommands_for(devcontainer)
        devcontainer['initializeCommand'] || []
      end

      # initialization script
      # @api private
      # @param devcontainer [Hash] devcontainer
      # @reutrn [Array<String>]
      def initscript_for(devcontainer)
        initializeCommands = []

        initializeCommands << precommands_for(devcontainer)
        initializeCommands << initcommands_for(devcontainer)
        initializeCommands << postcommands_for(devcontainer)

        script = initializeCommands.flatten.join(" && ")
        script
      end
    end
  end
end
