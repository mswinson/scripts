module Workspace
  module DOM
    class RepoElement < ::DOM::Element
      attr_accessor :name
      attr_accessor :source
      attr_accessor :dest
      
      def self.attributes
        {
          'name' => nil,
          'source' => nil,
          'dest' => nil
        }
      end
    end
  end
end
