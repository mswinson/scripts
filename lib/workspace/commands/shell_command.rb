module Workspace
  module Commands
    class ShellCommand < Command
      # run shell command
      # @api private
      # @param args [Array<String>] command arguments
      # @return void
      def cmd(args)
        system(args.join(' '))
      end
    end
  end
end

